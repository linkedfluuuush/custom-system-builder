# Planned Features

## Labels

- 🚧️ : In progress
- ✔️ : Done waiting for release
- 💤️ : Waiting for development
- ❌️ : Postponed / Aborted

## Planned

- Custom Active Effects
- Image component
- Element spanning across multiple columns in grid panels & tables
