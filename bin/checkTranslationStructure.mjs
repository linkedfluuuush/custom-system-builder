import fs from 'fs';

const TRANSLATION_FOLDER = './src/lang';
const REFERENCE_FILE_PATH = TRANSLATION_FOLDER + '/en.json';

const REFERENCE_FILE = JSON.parse(fs.readFileSync(REFERENCE_FILE_PATH, { encoding: 'utf-8' }));

const fileList = fs.readdirSync(TRANSLATION_FOLDER);

let returnCode = 0;

for (const fileName of fileList) {
    const filePath = TRANSLATION_FOLDER + '/' + fileName;

    if (filePath === REFERENCE_FILE_PATH) {
        continue;
    }

    const translationFile = JSON.parse(fs.readFileSync(filePath, { encoding: 'utf-8' }));
    const differences = compareJsonStructure(REFERENCE_FILE, translationFile);

    if (differences.length === 0) {
        console.log(`${fileName} is OK`);
    } else {
        console.log(`${fileName} structure is KO : \n\t${differences.join('\n\t')}\n`);
        returnCode = 1;
    }
}

// eslint-disable-next-line no-undef
process.exit(returnCode);

function compareJsonStructure(jsonRef, jsonActual, basePath = '') {
    const differences = [];
    const keysRef = Object.keys(jsonRef);
    const keysActual = Object.keys(jsonActual);

    const missingKeys = keysRef.filter((x) => !keysActual.includes(x));
    const addedKeys = keysActual.filter((x) => !keysRef.includes(x));

    differences.push(...missingKeys.map((key) => `Missing key ${basePath}${key}`));
    differences.push(...addedKeys.map((key) => `Added key ${basePath}${key}`));

    for (const key of keysRef) {
        if (keysActual.includes(key)) {
            if (typeof jsonRef[key] === 'object' && typeof jsonActual[key] === 'object') {
                differences.push(...compareJsonStructure(jsonRef[key], jsonActual[key], `${basePath}${key}.`));
            } else {
                if (typeof jsonRef[key] !== typeof jsonActual[key]) {
                    differences.push(
                        `Type mismatch for key ${basePath}${key}, is ${typeof jsonRef[
                            key
                        ]} in reference and ${typeof jsonActual[key]} in actual`
                    );
                }
            }
        }
    }

    return differences;
}
