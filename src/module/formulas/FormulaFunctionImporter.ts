import { DefinedPrimitive, JSONObject, JSONValue, Primitive } from '../definitions.js';
import { CustomItem } from '../documents/item.js';
import { DefferedUpdate } from './ComputablePhrase.js';
import { castToPrimitive, quoteString } from '../utils.js';
import Logger from '../Logger.js';
import Formula from './Formula.js';
import { CustomActor } from '../documents/actor.js';
import { UncomputableError } from '../errors/UncomputableError.js';
import { ExtensibleTableSystemProps } from '../sheets/components/ExtensibleTable.js';
import { MathJsInstance } from 'mathjs';

type FormulaFunctionOptions = {
    reference?: string;
    defaultValue?: Primitive;
    source: string;
    formula: string;
    computedTokens: { [p: string]: JSONValue };
    triggerEntity?: TemplateSystem;
    linkedEntity?: CustomItem;
    updates: Record<string, DefferedUpdate>;
    availableKeys: string[];
};

type CustomFunctions = {
    sameRow(columnName: string, fallbackValue: Primitive): Primitive;
    sameRowRef(columnName: string): string;
    sameRowIndex(): number;
    lookupRef(
        extensibleTableKey: string,
        targetColumn: string,
        filterColumn: string | null,
        filterValue: Primitive
    ): string[];
    lookup(
        extensibleTableKey: string,
        targetColumn: string,
        filterColumn: string | null,
        filterValue: Primitive,
        comparisonOperator: string
    ): Primitive[];
    alookup(extensibleTableKey: string, targetColumn: string, ...filterArgs: Primitive[]): Primitive[];
    lookupRange(
        extensibleTableKey: string,
        targetColumn: string,
        lookupColumn: string,
        value: number,
        opts: {
            orderBy: 'ASC' | 'DESC';
            isSorted: boolean;
            isInclusive: boolean;
        }
    ): Primitive;
    find(
        extensibleTableKey: string,
        targetColumn: string,
        filterColumn: string,
        filterValue: Primitive,
        comparisonOperator: string
    ): Primitive;
    first(list: Primitive[], fallbackValue: Primitive): Primitive;
    fallback(value: Primitive, fallback: Primitive): Primitive;
    consoleLog<T extends JSONObject>(dataLog: T): T;
    consoleTable<T extends JSONObject>(dataLog: T): T;
    ref(valueRef: string, fallbackValue: Primitive): Primitive;
    replace(text: string, pattern: string, replacement: string): string;
    replaceAll(text: string, pattern: string, replacement: string): string;
    concat(...values: Primitive[]): string;
    recalculate(userInputData: string): Primitive;
    fetchFromParent(formula: string, opt?: { fallback?: Primitive; top?: boolean }): Primitive;
    fetchFromActor(actorName: string, formula: string, fallbackValue: Primitive): Primitive;
    fetchFromUuid(uuid: string, formula: string, fallback: Primitive): Primitive;
    switchCase(expression: Primitive, ...args: Primitive[]): Primitive;
    setPropertyInEntity(
        entityName: string,
        propertyName: string | string[],
        formula: string,
        fallbackValue: Primitive | null
    ): Primitive;
    setValues(uuids: string | string[], ...updateArgs: string[]): string;
    notify(messageType: string, message: string): string;
    escapeQuotes(value: string): string;
    array(...values: Primitive[]): Primitive[];
};

export class FormulaFunctionImporter {
    public static importCustomFunctions(
        mathInstance: MathJsInstance,
        props: JSONObject,
        options: FormulaFunctionOptions
    ): CustomFunctions {
        const customFunctions: CustomFunctions = {
            sameRow: (columnName: string, fallbackValue: Primitive = null): Primitive => {
                const fullReference = `${options.reference}.${columnName}`;

                // Fetching the value inside dynamic table's row
                const returnValue =
                    castToPrimitive(foundry.utils.getProperty(props, fullReference)) ??
                    fallbackValue ??
                    options.defaultValue;

                if (returnValue === undefined) {
                    throw new UncomputableError(
                        `Uncomputable token "sameRow(${quoteString(
                            columnName
                        )})". Failed to obtain value from the same row in the column "${columnName}". Make sure, that the column-key is correct.`,
                        options.source,
                        options.formula,
                        props
                    );
                }

                options.computedTokens[`sameRow(${quoteString(columnName)})`] = returnValue;

                return returnValue;
            },
            sameRowRef: (columnName: string): string => {
                return `${options.reference}.${columnName}`;
            },
            sameRowIndex: (): number => {
                if (!options.reference) {
                    throw new UncomputableError(
                        '"sameRowIndex()" can only be used in referencable Tables',
                        options.source,
                        options.formula,
                        props
                    );
                }

                const referenceParts = options.reference.split('.');
                const dynamicTableProps: ExtensibleTableSystemProps = foundry.utils.getProperty(
                    props,
                    referenceParts[0]
                );
                const rowKeys = Object.entries(dynamicTableProps)
                    .filter(([_, row]) => !row.$deleted)
                    .map(([key, _]) => key);

                return Number(rowKeys.findIndex((row) => row === referenceParts[1]));
            },
            lookupRef: (
                extensibleTableKey: string,
                targetColumn: string,
                filterColumn: string | null = null,
                filterValue: Primitive = null
            ): string[] => {
                const extensibleTableProps: ExtensibleTableSystemProps = foundry.utils.getProperty(
                    props,
                    extensibleTableKey
                );

                if (extensibleTableProps === undefined) {
                    throw new UncomputableError(
                        `Uncomputable token "lookupRef(${quoteString(extensibleTableKey)}, ${quoteString(
                            targetColumn
                        )}, ${quoteString(filterColumn)}, ${quoteString(filterValue)})"`,
                        options.source,
                        options.formula,
                        props
                    );
                }

                return Object.entries(extensibleTableProps)
                    .filter(([_, row]) => (!row.$deleted && filterColumn ? row[filterColumn] === filterValue : true))
                    .map(([key]) => `${extensibleTableKey}.${key}.${targetColumn}`);
            },
            lookup: (
                extensibleTableKey: string,
                targetColumn: string,
                filterColumn: string | null = null,
                filterValue: Primitive = null,
                comparisonOperator = '==='
            ): Primitive[] => {
                let values: Primitive[] = [];

                let filterFunction: (elt: Record<string, Primitive>) => boolean | RegExpMatchArray | null = () => true;

                if (filterColumn) {
                    filterFunction = (elt) => {
                        switch (comparisonOperator) {
                            case '===':
                                return elt[filterColumn] === filterValue;
                            case '==':
                                return elt[filterColumn] == filterValue;
                            case '>':
                                return Number(elt[filterColumn]) > Number(filterValue);
                            case '>=':
                                return Number(elt[filterColumn]) >= Number(filterValue);
                            case '<':
                                return Number(elt[filterColumn]) < Number(filterValue);
                            case '<=':
                                return Number(elt[filterColumn]) <= Number(filterValue);
                            case '!==':
                                return elt[filterColumn] !== filterValue;
                            case '!=':
                                return elt[filterColumn] != filterValue;
                            case '~':
                                return String(elt[filterColumn]).match(String(filterValue));
                            default:
                                Logger.error(`"${comparisonOperator}" is not a valid comparison operator.`);
                                return false;
                        }
                    };
                }

                const extensibleTableProps: ExtensibleTableSystemProps = foundry.utils.getProperty(
                    props,
                    extensibleTableKey
                );
                const extensibleTablePropsArray = Object.values(extensibleTableProps ?? {})
                    .filter((row) => !row.$deleted)
                    .map((row) => {
                        const filteredRow: Record<string, Primitive> = {};

                        for (const prop of Object.keys(row).filter((key) => !key.startsWith('$'))) {
                            filteredRow[prop] = row[prop];
                        }

                        return filteredRow;
                    });

                if (
                    extensibleTablePropsArray.length > 0 &&
                    !extensibleTablePropsArray.some(
                        (row) =>
                            Object.keys(row).length > 0 && row[targetColumn] !== undefined && row[targetColumn] !== null
                    )
                ) {
                    throw new UncomputableError(
                        `Uncomputable token "lookup(${quoteString(extensibleTableKey)}, ${quoteString(
                            targetColumn
                        )}, ${quoteString(filterColumn)}, ${quoteString(filterValue)}, ${quoteString(
                            comparisonOperator
                        )})"`,
                        options.source,
                        options.formula,
                        props
                    );
                }

                values = extensibleTablePropsArray
                    .filter((row) => !row.$deleted && filterFunction(row))
                    .map((row) => castToPrimitive(row[targetColumn]))
                    .filter((value) => value !== undefined && value !== null);

                options.computedTokens[
                    `lookup(${quoteString(extensibleTableKey)}, ${quoteString(targetColumn)}, ${quoteString(
                        filterColumn
                    )}, ${quoteString(filterValue)}, ${quoteString(comparisonOperator)})`
                ] = values;

                return values;
            },
            alookup: (extensibleTableKey: string, targetColumn: string, ...filterArgs: Primitive[]): Primitive[] => {
                if (filterArgs.length % 3 != 0) {
                    throw new UncomputableError(
                        `The amount of filter arguments must be a multiple of 3, but you have only ${filterArgs.length}`,
                        options.source,
                        options.formula,
                        props
                    );
                }

                let values: Primitive[] = [];

                const extensibleTableProps: ExtensibleTableSystemProps = foundry.utils.getProperty(
                    props,
                    extensibleTableKey
                );

                let extensibleTablePropsArray = Object.values(extensibleTableProps ?? {})
                    .filter((row) => !row.$deleted)
                    .map((row) => {
                        const filteredRow: Record<string, Primitive> = {};

                        for (const prop of Object.keys(row).filter((key) => !key.startsWith('$'))) {
                            filteredRow[prop] = row[prop];
                        }

                        return filteredRow;
                    });

                if (
                    extensibleTablePropsArray.length > 0 &&
                    !extensibleTablePropsArray.some(
                        (row) =>
                            Object.keys(row).length > 0 && row[targetColumn] !== undefined && row[targetColumn] !== null
                    )
                ) {
                    throw new UncomputableError(
                        `Uncomputable token "alookup(${quoteString(extensibleTableKey)}, ${quoteString(targetColumn)},
                            ${filterArgs.map((arg) => `'${arg}'`).join(', ')})"`,
                        options.source,
                        options.formula,
                        props
                    );
                }

                for (let i = 0; i < filterArgs.length / 3; i++) {
                    const filterColumn = filterArgs[i * 3] as string;
                    const comparisonOperator = filterArgs[i * 3 + 1] as string;
                    const filterValue = filterArgs[i * 3 + 2];

                    let filterFunction: (elt: Record<string, Primitive>) => boolean | RegExpMatchArray | null = () =>
                        true;

                    if (filterColumn) {
                        filterFunction = (elt) => {
                            switch (comparisonOperator) {
                                case '===':
                                    return elt[filterColumn] === filterValue;
                                case '==':
                                    return elt[filterColumn] == filterValue;
                                case '>':
                                    return Number(elt[filterColumn]) > Number(filterValue);
                                case '>=':
                                    return Number(elt[filterColumn]) >= Number(filterValue);
                                case '<':
                                    return Number(elt[filterColumn]) < Number(filterValue);
                                case '<=':
                                    return Number(elt[filterColumn]) <= Number(filterValue);
                                case '!==':
                                    return elt[filterColumn] !== filterValue;
                                case '!=':
                                    return elt[filterColumn] != filterValue;
                                case '~':
                                    return String(elt[filterColumn]).match(String(filterValue));
                                default:
                                    Logger.error(`"${comparisonOperator}" is not a valid comparison operator.`);
                                    return false;
                            }
                        };
                    }

                    extensibleTablePropsArray = extensibleTablePropsArray.filter((row) => filterFunction(row));
                }

                values = extensibleTablePropsArray
                    .map((row) => castToPrimitive(row[targetColumn]))
                    .filter((value) => value !== undefined && value !== null);

                options.computedTokens[
                    `alookup(${quoteString(extensibleTableKey)}, ${quoteString(targetColumn)},
                            ${filterArgs.map((arg) => `'${arg}'`).join(', ')})`
                ] = values;

                return values;
            },
            lookupRange(
                extensibleTableKey: string,
                targetColumn: string,
                lookupColumn: string,
                value: number,
                opts: {
                    orderBy: 'ASC' | 'DESC';
                    isSorted: boolean;
                    isInclusive: boolean;
                } = {
                    orderBy: 'ASC',
                    isSorted: false,
                    isInclusive: true
                }
            ): Primitive {
                const extensibleTableProps: ExtensibleTableSystemProps = foundry.utils.getProperty(
                    props,
                    extensibleTableKey
                );

                const extensibleTablePropsArray = Object.values(extensibleTableProps ?? {})
                    .filter((row) => !row.$deleted)
                    .map((row) => {
                        const filteredRow: Record<string, Primitive> = {};

                        for (const prop of Object.keys(row).filter((key) => !key.startsWith('$'))) {
                            filteredRow[prop] = row[prop];
                        }

                        return filteredRow;
                    }) as Record<string, number>[];

                if (
                    extensibleTablePropsArray.length > 0 &&
                    !extensibleTablePropsArray.some(
                        (row) =>
                            Object.keys(row).length > 0 && row[targetColumn] !== undefined && row[targetColumn] !== null
                    )
                ) {
                    throw new UncomputableError(
                        `Uncomputable token "lookupRange(${quoteString(extensibleTableKey)}, ${quoteString(
                            targetColumn
                        )}, ${quoteString(lookupColumn)}, ${quoteString(value)}, 
                            {orderBy: ${quoteString(opts.orderBy)}, isSorted: ${opts.isSorted}, isInclusive: ${
                            opts.isInclusive
                        }})"`,
                        options.source,
                        options.formula,
                        props
                    );
                }

                if (!opts.isSorted) {
                    const compareSort: (a: Record<string, number>, b: Record<string, number>) => number =
                        opts.orderBy === 'ASC'
                            ? (a, b) => a[lookupColumn] - b[lookupColumn]
                            : (a, b) => b[lookupColumn] - a[lookupColumn];

                    extensibleTablePropsArray.sort(compareSort);
                }

                const hasReachedRange: (a: number, b: number) => boolean =
                    opts.orderBy === 'ASC'
                        ? opts.isInclusive
                            ? (a, b) => a <= b
                            : (a, b) => a < b
                        : opts.isInclusive
                        ? (a, b) => a >= b
                        : (a, b) => a > b;

                const returnRow = extensibleTablePropsArray
                    .reverse()
                    .find((row) => hasReachedRange(Number(row[lookupColumn]), Number(value)));

                const returnValue = returnRow ? returnRow[targetColumn] : null;

                options.computedTokens[
                    `lookupRange(${quoteString(extensibleTableKey)}, ${quoteString(targetColumn)},
                            ${quoteString(lookupColumn)}, ${quoteString(value)}, 
                            {orderBy: ${quoteString(opts.orderBy)}, isSorted: ${opts.isSorted}, isInclusive: ${
                        opts.isInclusive
                    }})`
                ] = returnValue;

                return returnValue;
            },
            find: (
                extensibleTableKey: string,
                targetColumn: string,
                filterColumn: string | null = null,
                filterValue: Primitive = null,
                comparisonOperator = '==='
            ): Primitive => {
                let filterFunction: (elt: Record<string, Primitive>) => boolean | RegExpMatchArray | null = () => true;

                if (filterColumn) {
                    filterFunction = (elt) => {
                        switch (comparisonOperator) {
                            case '===':
                                return elt[filterColumn] === filterValue;
                            case '==':
                                return elt[filterColumn] == filterValue;
                            case '>':
                                return Number(elt[filterColumn]) > Number(filterValue);
                            case '>=':
                                return Number(elt[filterColumn]) >= Number(filterValue);
                            case '<':
                                return Number(elt[filterColumn]) < Number(filterValue);
                            case '<=':
                                return Number(elt[filterColumn]) <= Number(filterValue);
                            case '!==':
                                return elt[filterColumn] !== filterValue;
                            case '!=':
                                return elt[filterColumn] != filterValue;
                            case '~':
                                return String(elt[filterColumn]).match(String(filterValue));
                            default:
                                Logger.error(`"${comparisonOperator}" is not a valid comparison operator.`);
                                return false;
                        }
                    };
                }

                const extensibleTableProps: ExtensibleTableSystemProps = foundry.utils.getProperty(
                    props,
                    extensibleTableKey
                );

                const returnRow = Object.values(extensibleTableProps ?? {}).find((row) => {
                    if (
                        Object.keys(row).length === 0 ||
                        row[targetColumn] === undefined ||
                        row[targetColumn] === null
                    ) {
                        throw new UncomputableError(
                            `Uncomputable token "find(${quoteString(extensibleTableKey)}, ${quoteString(
                                targetColumn
                            )}, ${quoteString(filterColumn)}, ${quoteString(filterValue)}, ${quoteString(
                                comparisonOperator
                            )})"`,
                            options.source,
                            options.formula,
                            props
                        );
                    }

                    return !row.$deleted && filterFunction(row);
                });

                const returnValue = returnRow ? returnRow[targetColumn] : null;

                options.computedTokens[
                    `find(${quoteString(extensibleTableKey)}, ${quoteString(targetColumn)}, ${quoteString(
                        filterColumn
                    )}, ${quoteString(filterValue)}, ${quoteString(comparisonOperator)})`
                ] = returnValue;

                return returnValue;
            },
            first: (list = [], fallbackValue: Primitive = null): Primitive => {
                let returnValue = fallbackValue ?? options.defaultValue;

                if (list.length > 0) {
                    returnValue = castToPrimitive(list[0]);
                }

                return returnValue;
            },
            fallback(value: Primitive, fallback: Primitive): Primitive {
                return value ?? fallback;
            },
            consoleLog: <T extends JSONObject>(dataLog: T): T => {
                console.log(dataLog);
                return dataLog;
            },
            consoleTable: <T extends JSONObject>(dataLog: T): T => {
                console.table(dataLog);
                return dataLog;
            },
            ref: (valueRef: string, fallbackValue: Primitive = null): Primitive => {
                let returnValue = fallbackValue ?? options.defaultValue;
                let realValue = undefined;

                if (valueRef) {
                    realValue = foundry.utils.getProperty(props, valueRef);
                    returnValue = castToPrimitive(realValue) ?? returnValue;
                }

                if (
                    returnValue === undefined ||
                    (realValue === undefined && options.availableKeys.includes(valueRef))
                ) {
                    throw new UncomputableError(
                        `Uncomputable token ref(${quoteString(valueRef)})`,
                        options.source,
                        options.formula,
                        props
                    );
                }

                options.computedTokens[`ref(${quoteString(valueRef)}, ${quoteString(fallbackValue)})`] = returnValue;

                return returnValue;
            },
            replace: (text: string, pattern: string, replacement: string): string => {
                return text.replace(pattern, replacement);
            },
            replaceAll: (text: string, pattern: string, replacement: string): string => {
                return text.replaceAll(pattern, replacement);
            },
            concat: (...values: DefinedPrimitive[]): string => {
                return ''.concat(...values.map(String));
            },
            recalculate: (userInputData: string): Primitive => {
                if (userInputData === undefined) {
                    throw new UncomputableError(
                        'Uncomputable token recalculate()',
                        options.source,
                        options.formula,
                        props
                    );
                }

                try {
                    const value = ComputablePhrase.computeMessageStatic(
                        userInputData.toString(),
                        props,
                        options
                    ).result;

                    return castToPrimitive(value);
                } catch (_err) {
                    throw new UncomputableError(
                        'Uncomputable token recalculate()',
                        options.source,
                        options.formula,
                        props
                    );
                }
            },
            fetchFromParent: (formula: string, opt?: { fallback?: Primitive; top?: boolean }): Primitive => {
                if (options.triggerEntity?.entity instanceof CustomItem) {
                    const parentEntity = opt?.top
                        ? options?.triggerEntity?.entity?.parent
                        : options?.triggerEntity?.entity?.getParent();

                    if (!parentEntity) {
                        return opt?.fallback ?? options.defaultValue;
                    }

                    const returnValue = castToPrimitive(
                        new Formula(formula).computeStatic(parentEntity.system.props, {
                            ...options,
                            defaultValue: opt?.fallback ?? options.defaultValue
                        }).result
                    );

                    mathInstance.import(this.importCustomFunctions(mathInstance, props, options), {
                        override: true
                    });

                    return returnValue;
                } else {
                    throw new UncomputableError(
                        'fetchFromParent() is only usable in Items',
                        options.source,
                        options.formula,
                        props
                    );
                }
            },
            fetchFromActor: (actorName: string, formula: string, fallbackValue: Primitive = null): Primitive => {
                let actor;
                switch (actorName) {
                    case 'selected':
                        actor = canvas?.tokens?.controlled[0]?.actor ?? game!.user!.character;
                        break;
                    case 'target':
                        actor = game!.user!.targets.values().next().value?.actor;
                        break;
                    case 'attached':
                        Logger.warn(
                            "fetchFromActor('attached', ...) is deprecated in favor of fetchFromParent(). It will be removed with V14",
                            options.source,
                            options.formula,
                            props
                        );
                        actor = options?.triggerEntity?.entity.parent;
                        break;
                    default:
                        actor = game!.actors!.filter((e) => e.name === actorName)[0];
                }

                // If actor was found
                if (actor) {
                    const actorProps = (actor as CustomActor).system.props;

                    const returnValue = castToPrimitive(
                        new Formula(formula).computeStatic(actorProps, {
                            ...options,
                            defaultValue: fallbackValue ?? options.defaultValue
                        }).result
                    );

                    mathInstance.import(this.importCustomFunctions(mathInstance, props, options), {
                        override: true
                    });

                    return returnValue;
                }

                return fallbackValue ?? options.defaultValue;
            },
            fetchFromUuid: (uuid: string, formula: string, fallback: Primitive = null): Primitive => {
                const entity = fromUuidSync(uuid);

                if (!(entity instanceof CustomActor || entity instanceof CustomItem)) {
                    return fallback ?? options.defaultValue;
                }

                if (!entity) {
                    return fallback ?? options.defaultValue;
                }

                const returnValue = castToPrimitive(
                    new Formula(formula).computeStatic(entity.system.props, {
                        ...options,
                        defaultValue: fallback ?? options.defaultValue
                    }).result
                );

                mathInstance.import(this.importCustomFunctions(mathInstance, props, options), {
                    override: true
                });

                return returnValue;
            },
            switchCase: (expression: Primitive, ...args: Primitive[]): Primitive => {
                while (args.length > 1 && args.shift() !== expression) {
                    args.shift();
                }
                return args.shift() ?? null;
            },
            setPropertyInEntity: (
                entityName: string,
                propertyName: string | string[],
                formula: string,
                fallbackValue: Primitive = null
            ): Primitive => {
                let entity: CustomActor | CustomItem | undefined;
                switch (entityName) {
                    case 'self':
                        if (!options.triggerEntity) {
                            throw new UncomputableError(
                                `Missing entity. Make sure, that you provide the triggering Entity in the Options of Formula or ComputablePhrase`,
                                options.source,
                                options.formula,
                                props
                            );
                        }
                        entity = options.triggerEntity.entity;
                        break;
                    case 'selected':
                        entity = (canvas?.tokens?.controlled[0]?.actor ?? game!.user!.character) as
                            | CustomActor
                            | undefined;
                        break;
                    case 'target':
                        entity = game.user!.targets.values().next().value?.actor as CustomActor;
                        break;
                    case 'attached':
                        entity = options?.triggerEntity?.entity.parent as CustomActor | undefined;
                        break;
                    case 'item':
                        if (!options.linkedEntity) {
                            throw new UncomputableError(
                                `Missing entity. Make sure, that you provide the linked Entity in the Options of Formula or ComputablePhrase`,
                                options.source,
                                formula,
                                props
                            );
                        }
                        entity = options.linkedEntity;
                        break;

                    default:
                        entity = game!.actors!.filter((e) => e.name === entityName)[0] as CustomActor | undefined;
                }

                if (!entity) {
                    throw new UncomputableError(
                        `Entity "${entityName}" not found`,
                        options.source,
                        options.formula,
                        props
                    );
                }

                const actorProps = entity.system.props;

                const value = castToPrimitive(
                    new Formula(formula).computeStatic(
                        { ...props, target: actorProps },
                        {
                            ...options,
                            defaultValue: fallbackValue ?? options.defaultValue
                        }
                    ).result
                );

                const uuid = entity.uuid.replaceAll('.', '-');

                const keys = typeof propertyName === 'string' ? [propertyName] : propertyName;
                const diff: { system: { props: Record<string, JSONValue> } } = { system: { props: {} } };
                keys.forEach((key) => (diff.system.props[key] = value));

                if (Object.keys(options.updates).some((key) => key === uuid)) {
                    foundry.utils.mergeObject(options.updates, { [uuid]: diff });
                } else {
                    options.updates[uuid] = diff;
                }

                mathInstance.import(this.importCustomFunctions(mathInstance, props, options), {
                    override: true
                });

                return value;
            },
            setValues: (uuid: string | string[], ...updateArgs: string[]): string => {
                if (updateArgs.length % 2 != 0) {
                    throw new UncomputableError(
                        `The amount of update arguments must be a multiple of 2, but you have only ${updateArgs.length}`,
                        options.source,
                        options.formula,
                        props
                    );
                }

                const uuids = typeof uuid === 'string' ? [uuid] : uuid;

                return uuids
                    .map((uuid) => {
                        const entity = fromUuidSync(uuid);
                        if (!(entity instanceof CustomActor || entity instanceof CustomItem)) {
                            Logger.error(`"${uuid}" couldn't be resolved to an Actor or Item`);
                            return `${uuid} => ERROR`;
                        }

                        const diff: { system: { props: Record<string, JSONValue> } } = { system: { props: {} } };
                        for (let i = 0; i < updateArgs.length / 2; i++) {
                            const key = updateArgs[i * 2];
                            const formula = updateArgs[i * 2 + 1];

                            diff.system.props[key] = castToPrimitive(
                                new Formula(formula).computeStatic({ ...props, target: entity.system.props }, options)
                                    .result
                            );
                        }

                        const formattedUuid = uuid.replaceAll('.', '-');
                        if (Object.keys(options.updates).some((key) => key === uuid)) {
                            foundry.utils.mergeObject(options.updates, { [formattedUuid]: diff });
                        } else {
                            options.updates[formattedUuid] = diff;
                        }

                        return `${entity.name} => ${JSON.stringify(diff.system.props)}`;
                    })
                    .join(', ');
            },
            notify: (messageType: string, message: string): string => {
                const validTypes = ['info', 'warn', 'error'];
                if (!validTypes.includes(messageType)) {
                    throw new UncomputableError(
                        `Message-Type "${messageType}" is not valid`,
                        options.source,
                        options.formula,
                        props
                    );
                }

                ui!.notifications[messageType as 'info' | 'warn' | 'error'](message);
                return message;
            },
            escapeQuotes: (value: string): string => {
                return value.replaceAll("'", "\\'");
            },
            array: (...values: Primitive[]): Primitive[] => {
                return values;
            }
        };

        return customFunctions;
    }
}
