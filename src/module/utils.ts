/*
 * Copyright 2024 Jean-Baptiste Louvet-Daniel
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

/**
 * @ignore
 * @module
 */

import { ChatMessageDataConstructorData } from '@league-of-foundry-developers/foundry-vtt-types/src/foundry/common/data/data.mjs/chatMessageData.js';
import { JSONObject, Primitive, TableJson } from './definitions.js';
import { CustomActor } from './documents/actor.js';
import { DICE_ROLL_MODES } from '@league-of-foundry-developers/foundry-vtt-types/src/foundry/common/constants.mjs.js';
import { CustomItem } from './documents/item.js';
import { ComponentJson } from './sheets/components/Component.js';
import { ContainerJson } from './sheets/components/Container.js';

/**
 * @ignore
 */
export const postCustomSheetRoll = async (messageText: string, alternative: boolean = false) => {
    const actor = (canvas!.tokens!.controlled[0]?.actor ?? game.user!.character) as CustomActor;

    if (
        actor &&
        actor.testUserPermission(game.user!, (CONST as unknown as CORRECTED_CONST).DOCUMENT_OWNERSHIP_LEVELS.OBSERVER)
    ) {
        try {
            actor.roll(messageText, { alternative });
        } catch (err) {
            ui.notifications.error((err as Error).message);
        }
    } else {
        ui.notifications.error(game.i18n.localize('CSB.ChatCommands.ActorNotFound'));
    }
};

/**
 * Posts a chat message with a computed phrase data
 * @ignore
 */
export const postAugmentedChatMessage = async (
    textContent: Pick<ComputablePhrase, 'buildPhrase' | 'values'>,
    msgOptions: ChatMessageDataConstructorData,
    { rollMode, create }: { rollMode?: DICE_ROLL_MODES; create: boolean } = { create: true }
) => {
    rollMode = rollMode || game.settings.get('core', 'rollMode');

    // chat-roll is just the html template for computed formulas
    const template_file = `systems/${game.system.id}/templates/chat/chat-roll.hbs`;
    const template_file_groups = `systems/${game.system.id}/templates/chat/chat-roll-dice-pools.hbs`;

    let phrase = textContent.buildPhrase;

    if (!phrase) {
        return;
    }

    const values = textContent.values;

    const rolls = [];
    const diceGroups = [];

    // Render all formulas HTMLs
    for (const key in values) {
        let formattedValue = String(values[key].result);
        if (values[key].explanation) {
            formattedValue = await renderTemplate(template_file, {
                rollData: values[key],
                jsonRollData: JSON.stringify(values[key]).replaceAll(/\[\[/g, '[').replaceAll(/]]/g, ']'),
                rollMode: rollMode
            });
        }

        if (phrase.startsWith('/')) {
            formattedValue = formattedValue.replaceAll(/"/g, '\\"');
        }

        // Using function for replace to ignore problems linked to '$' character in replace function
        // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/replace#specifying_a_string_as_the_replacement
        phrase = phrase.replace(key, () => formattedValue);

        for (const roll of values[key].rolls) {
            const rollObject = Roll.fromData(roll.roll);
            rolls.push(rollObject);

            if (roll.expandRollInChatCard) {
                diceGroups.push(...getDiceGroupsFromRoll(rollObject, values[key].hidden));
            }
        }
    }

    phrase = phrase.replaceAll(/\n/g, '').trim();

    const diceGroupsRendered = await renderTemplate(template_file_groups, {
        dice: diceGroups,
        multipleDice: diceGroups.filter((group) => !group.hidden).length > 1
    });

    phrase += diceGroupsRendered;

    const chatRollData: Partial<ChatMessageDataConstructorData> & {
        rollMode?: string;
    } = {
        // @ts-expect-error Foundry not up to date in v12 yet
        rolls: rolls.map((roll) => roll.toJSON())
    };

    let whisper = null;

    // If setting expandRollVisibility is checked, we apply the appropriate whispers to the message
    if (game.settings.get(game.system.id, 'expandRollVisibility')) {
        const gmList = ChatMessage.getWhisperRecipients('GM');

        chatRollData.rollMode = CONST.DICE_ROLL_MODES.PUBLIC;
        switch (rollMode) {
            case CONST.DICE_ROLL_MODES.PRIVATE:
                whisper = gmList;
                break;
            case CONST.DICE_ROLL_MODES.BLIND:
                whisper = gmList;
                break;
            case CONST.DICE_ROLL_MODES.SELF:
                whisper = [game.user];
                break;
            default:
                break;
        }
    } else {
        chatRollData.rollMode = rollMode;
    }

    const chatData = foundry.utils.mergeObject(
        msgOptions,
        foundry.utils.mergeObject(
            {
                content: phrase,
                whisper,
                sound: CONFIG.sounds.dice
            },
            chatRollData
        )
    );

    if (create) {
        // Final chat message creation
        if (Hooks.call('chatMessage', ui.chat, phrase, chatData) === false) return;

        return ChatMessage.create(chatData);
    } else {
        const msg = new ChatMessage(chatData);

        if (rollMode) {
            msg.applyRollMode(rollMode);
        }
        return msg.toObject();
    }
};

/**
 * @ignore
 */
export const removeNull = (obj: Record<string | number, unknown>) => {
    const newObj: Record<string | number | symbol, unknown> = {};
    Object.keys(obj).forEach((key) => {
        if (obj[key] === Object(obj[key]))
            newObj[key] = removeNull(obj[key] as Record<string | number | symbol, unknown>);
        else if (obj[key] !== null) newObj[key] = obj[key];
    });
    return newObj;
};

/**
 * @param value
 */
export const castToPrimitive = (value: Primitive): Primitive => {
    if (typeof value === 'string') {
        if (value === 'true') {
            return true;
        }
        if (value === 'false') {
            return false;
        }

        const numericValue = parseInt(value);
        if (!Number.isNaN(numericValue) && /^[-+]?\d+$/.test(value)) {
            return numericValue;
        }
    }

    return value;
};

export const getGameCollectionAsTemplateSystems = (
    collectionType: 'actor' | 'item' | undefined
): Array<TemplateSystem> => {
    switch (collectionType) {
        case 'actor':
            return game.actors!.map((actor: CustomActor) => actor.templateSystem);
        case 'item':
            return game.items!.map((item: CustomItem) => item.templateSystem);
        default:
            throw new Error(`Unknown entity type ${collectionType}`);
    }
};

export const getGameCollection = (collectionType: 'actor' | 'item' | undefined): Actors | Items => {
    switch (collectionType) {
        case 'actor':
            return game.actors!;
        case 'item':
            return game.items!;
        default:
            throw new Error(`Unknown entity type ${collectionType}`);
    }
};

export const getLocalizedRoleList = (keyType: 'number' | 'string'): object => {
    return Object.fromEntries(
        Object.entries(CONST.USER_ROLES).map(([key, value]) => [
            keyType == 'number' ? value : key,
            game.i18n.localize(`USER.Role${key.toLowerCase().capitalize()}`)
        ])
    );
};

export const getLocalizedPermissionList = (keyType: 'number' | 'string'): object => {
    return Object.fromEntries(
        Object.entries((CONST as unknown as CORRECTED_CONST).DOCUMENT_OWNERSHIP_LEVELS).map(([key, value]) => [
            keyType == 'number' ? value : key,
            game.i18n.localize(`OWNERSHIP.${key}`)
        ])
    );
};

export type Alignment = 'left' | 'center' | 'right' | 'justify';

export const getLocalizedAlignmentList = (includeJustify: boolean = false): Record<Alignment, string> => {
    const alignments: Record<string, string> = {
        left: game.i18n.localize('CSB.ComponentProperties.Alignment.AlignLeft'),
        center: game.i18n.localize('CSB.ComponentProperties.Alignment.AlignCenter'),
        right: game.i18n.localize('CSB.ComponentProperties.Alignment.AlignRight')
    };

    if (includeJustify) {
        alignments.justify = game.i18n.localize('CSB.ComponentProperties.Alignment.AlignJustify');
    }

    return alignments;
};

export const updateKeysOnCopy = (
    components: Array<ComponentJson | null>,
    availableKeys: Set<string>
): Array<ComponentJson | null> => {
    for (const component of components) {
        //TODO remove this check once Table is composed of sub Components for Rows
        if (component) {
            if (component.key && availableKeys.has(component.key)) {
                let suffix = 1;
                const originalKey = component.key;

                do {
                    component.key = originalKey + '_copy' + suffix;
                    suffix++;
                } while (availableKeys.has(component.key));
            }

            if ((component as ContainerJson).contents && (component as ContainerJson).contents.length > 0) {
                if (Array.isArray((component as TableJson).contents[0])) {
                    //TODO remove this once Table is composed of sub Components for Rows
                    (component as TableJson).contents = (component as TableJson).contents.map((row) => {
                        return updateKeysOnCopy(row, availableKeys);
                    });
                } else {
                    (component as ContainerJson).contents = updateKeysOnCopy(
                        (component as ContainerJson).contents,
                        availableKeys
                    ) as Array<ComponentJson>;
                }
            }
        }
    }

    return components;
};

export function quoteString(value: Primitive): string | null | undefined {
    if (value && typeof value === 'string') {
        return `'${value}'`;
    }

    return value === null ? null : value?.toString();
}

export function fastSetFlag(scope: string, key: string, value: unknown) {
    game.user!.setFlag(scope, key, value);
    foundry.utils.setProperty(game.user!.flags, `${scope}.${key}`, value);
}

export async function createProseMirrorEditor(container: HTMLElement, contents: string, options: JSONObject = {}) {
    options = foundry.utils.mergeObject(
        {
            collaborate: false,
            plugins: {
                //@ts-expect-error Outdated types
                highlightDocumentMatches: ProseMirror.ProseMirrorHighlightMatchesPlugin.build(
                    //@ts-expect-error Outdated types
                    ProseMirror.defaultSchema
                )
            }
        },
        options
    );

    //@ts-expect-error Outdated types
    return ProseMirrorEditor.create(container, contents, options);
}

export function trimProseMirrorEmptyValue(value?: string): string {
    if (value === undefined) {
        return '';
    }

    const htmlValue = $(`<div>${value}</div>`);
    htmlValue.find('br.ProseMirror-trailingBreak').remove();
    htmlValue.find('.ProseMirror-selectednode').removeClass('ProseMirror-selectednode').removeAttr('draggable');
    value = htmlValue.html();

    if (value === '<p></p>') {
        return '';
    }

    return value;
}

type DicePool = {
    dice: Array<{
        faces: number;
        result: string;
        classes: string;
    }>;
    formula: string;
    total: number;
    flavor?: string;
    icon?: string;
    method?: string;
    hidden: boolean;
};

export function getDiceGroupsFromRoll(roll: Roll, hidden?: boolean): Array<DicePool> {
    const groups = [];

    const currentPool: DicePool = { dice: [], formula: '', total: 0, hidden: hidden ?? false };
    let operator = '';

    for (const term of roll.terms) {
        if ((term as PoolTerm).rolls) {
            for (const roll of (term as PoolTerm).rolls) {
                groups.push(...getDiceGroupsFromRoll(roll, hidden));
            }
        }

        // @ts-expect-error Outdated types
        if (term instanceof foundry.dice.terms.ParentheticalTerm) {
            groups.push(...getDiceGroupsFromRoll((term as ParentheticalTerm).roll, hidden));
        }

        // @ts-expect-error Outdated types
        if (term instanceof foundry.dice.terms.DiceTerm) {
            currentPool.formula += operator + term.expression;
            currentPool.total += term.total as number;
            const tooltip = (term as DiceTerm).getTooltipData();
            currentPool.flavor = tooltip.flavor;
            // @ts-expect-error Outdated types
            currentPool.icon = tooltip.icon;
            // @ts-expect-error Outdated types
            currentPool.method = tooltip.method;

            for (const roll of tooltip.rolls) {
                currentPool.dice.push({
                    faces: tooltip.faces,
                    result: roll.result,
                    classes: roll.classes
                });
            }
            // @ts-expect-error Outdated types
        } else if (term instanceof foundry.dice.terms.OperatorTerm) {
            operator = term.formula;
        }
    }

    if (currentPool.dice.length > 0) {
        groups.push(currentPool);
    }

    return groups;
}
