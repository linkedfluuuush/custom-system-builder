/*
 * Copyright 2024 Jean-Baptiste Louvet-Daniel
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

import type TemplateSystemClass from './documents/templateSystem.js';
import type ComputablePhraseClass from './formulas/ComputablePhrase.js';
import type Modifier from './interfaces/Modifier.js';
import type Component from './sheets/components/Component.js';
import type { ComponentJson } from './sheets/components/Component.js';
import type { ComponentFactory } from './sheets/components/ComponentFactory.js';
import type { ContainerJson } from './sheets/components/Container.js';
import { EffectChangeData } from '@league-of-foundry-developers/foundry-vtt-types/src/foundry/common/data/data.mjs/effectChangeData.js';
import { CustomActor } from './documents/actor.js';
import { CustomItem } from './documents/item.js';

/* eslint-disable no-var */
declare global {
    var TemplateSystem: typeof TemplateSystemClass;
    interface TemplateSystem extends InstanceType<typeof TemplateSystem> {}

    var ComputablePhrase: typeof ComputablePhraseClass;
    interface ComputablePhrase extends InstanceType<typeof ComputablePhrase> {}

    var componentFactory: ComponentFactory;

    var copiedComponent: { sourceId: string; component: Component } | null;

    var DeepDiff: {
        diff: (d1: JSONObject, d2: JSONObject) => Array<JSONObject>;
        applyChange: (target: JSONObject, source: JSONObject, change: JSONObject) => void;
        revertChange: (target: JSONObject, source: JSONObject, change: JSONObject) => void;
    };

    interface DocumentClassConfig {
        Actor: CustomActor;
        Item: CustomItem;
    }

    // Type stubs for FoundryV11
    interface Actor {
        system: System;
        flags: JSONObject;
        statuses: Array<string>;
        get appliedEffects(): ActiveEffect[];
    }
    interface Item {
        system: System;
        flags: JSONObject;
        statuses: Array<string>;
    }
    interface ActiveEffect {
        changes: EffectChangeData[];
        img: string;
    }
    interface StatusEffect {
        id: string;
        name: string;
        img: string;
    }

    interface Folder {
        children: Folder[];
    }

    interface User {
        flags: JSONObject;
    }
    interface TokenDocument {
        flags: JSONObject;
    }
    interface Combat {
        flags: JSONObject;
    }
    interface Combatant {
        flags: JSONObject;
    }
    interface Scene {
        flags: JSONObject;
    }

    interface LenientGlobalVariableTypes {
        game: never;
        ui: never;
    }

    interface CONFIG {
        statusEffects: StatusEffect[];
    }

    interface CORRECTED_CONST {
        DOCUMENT_OWNERSHIP_LEVELS: Readonly<{
            NONE: 0;
            LIMITED: 1;
            OBSERVER: 2;
            OWNER: 3;
        }>;
    }

    var CORRECTED_CONST: typeof CONST & CORRECTED_CONST;

    function fromUuidSync(uuid: string): Document;

    var focusedApp: string;
}

/* eslint-enable no-var */

export type DefinedPrimitive = boolean | number | string;
export type Primitive = boolean | null | number | string | undefined;

export type JSONValue = Primitive | JSONObject | JSONArray;

export interface JSONObject {
    [key: string]: JSONValue;
}

export interface JSONArray extends Array<JSONValue> {}

export interface DeepNestedObject<T> {
    [key: string]: T | DeepNestedObject<T>;
}

export type Optional<T, K extends keyof T> = Pick<Partial<T>, K> & Omit<T, K>;

/* CSB SPECIFIC TYPES */

export type System = {
    container?: string; // Id of the container item if in an item contained in another item
    templateSystemUniqueVersion?: number;
    props: JSONObject;
    attributeBar: Record<string, AttributeBar | UnresolvedAttributeBar>;
    hidden: Array<HiddenProp>;
    activeConditionalModifierGroups: Array<string>;
    activeEffects: Record<string, Array<Modifier>>;
    header: JSONObject;
    body: JSONObject;
    unique?: boolean;
    uniqueId?: string;
    display: JSONObject;
    template?: string;
    modifiers?: Array<Modifier>;
};

export type HiddenProp = {
    name: string;
    value: string;
};

export type AttributeBar = {
    key: string;
    max: number;
    value?: number;
    editable: boolean;
};

export type UnresolvedAttributeBar = {
    max: string;
    value?: string | number;
    editable: boolean;
};

export type RollOptions = {
    postMessage?: boolean;
    alternative?: boolean;
};

export type TableJson = Omit<ContainerJson, 'contents'> & {
    contents: Array<Array<ComponentJson | null>>;
    cols: number;
    rows: number;
    layout?: string;
};

export type CustomSystemExport = {
    isCustomSystemExport: boolean;
    actors: CustomActorExport[];
    items: CustomItemExport[];
};

export type CustomActorExport = {
    id?: string;
    type: '_template';
    name: string;
    data: System;
    flags: Record<string, unknown>;
};

export type CustomItemExport = {
    id?: string;
    type: 'subTemplate' | '_equippableItemTemplate' | 'userInputTemplate';
    name: string;
    data: System;
    flags: Record<string, unknown>;
};
