import type { Primitive } from '../definitions.js';

export default interface Modifier {
    id: string;
    conditionalGroup?: string;
    priority: number;
    key: string;
    operator: MODIFIER_OPERATOR;
    formula: string;
    description?: string;
    originalEntity?: TemplateSystem;
    value?: string;
    isSelected?: boolean;
}

export enum MODIFIER_OPERATOR {
    SET = 'set',
    ADD = 'add',
    SUBTRACT = 'subtract',
    MULTIPLY = 'multiply',
    DIVIDE = 'divide'
}

export const MODIFIER_OPERATORS = {
    [MODIFIER_OPERATOR.ADD]: '+',
    [MODIFIER_OPERATOR.MULTIPLY]: 'x',
    [MODIFIER_OPERATOR.SUBTRACT]: '-',
    [MODIFIER_OPERATOR.DIVIDE]: '/',
    [MODIFIER_OPERATOR.SET]: '='
};

export const applyModifiers = (value: Primitive, modifiers: Modifier[] = []) => {
    const filteredModifiers = modifiers
        .filter((modifier) => !!modifier.isSelected)
        .sort((mod1, mod2) => {
            const operatorOrder = ['set', 'multiply', 'divide', 'add', 'subtract'];

            let sortIndex = mod1.priority - mod2.priority;

            if (sortIndex === 0) {
                sortIndex = operatorOrder.indexOf(mod1.operator) - operatorOrder.indexOf(mod2.operator);
            }

            return sortIndex;
        });

    for (const modifier of filteredModifiers) {
        switch (modifier.operator) {
            case MODIFIER_OPERATOR.SET:
                if (String(modifier.value) !== '') {
                    value = isNaN(Number(modifier.value)) ? String(modifier.value) : Number(modifier.value);
                }
                break;
            case MODIFIER_OPERATOR.MULTIPLY:
                value = Number(value) * Number(modifier.value);
                break;
            case MODIFIER_OPERATOR.DIVIDE:
                value = Number(value) / Number(modifier.value);
                break;
            case MODIFIER_OPERATOR.SUBTRACT:
                value = Number(value) - Number(modifier.value);
                break;
            case MODIFIER_OPERATOR.ADD:
            default:
                value = Number(value) + Number(modifier.value);
                break;
        }
    }

    return value;
};
