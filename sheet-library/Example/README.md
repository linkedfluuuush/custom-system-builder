# This version is no longer maintained!
I've moved the Example-Sheet into a module for convenience. You can now find it
[here](https://foundryvtt.com/packages/custom-system-builder-example). It is also available in the Package Manager of
your Foundry Application. You'll find it under the name "Custom System Builder - Example". With this, the Example-Sheet
in this library **will no longer be maintained!** So please refer to the alternative version for now on.

## How to import the Example-Template in Foundry
- Download this folder by clicking the download-icon and select `Download this directory`
- Extract the contents of the downloaded folder
- Go to Foundry to the `Game Settings`-Tab and click `Import templates JSON`. In there you select the
`Example_VXXX_EN_Martin1522.json`-file and import it

## How to import the Items
- Go to Foundry to the `Items`-Tab and click on `Create Item`. It doesn´t matter which name or type you enter, just
create a new one.
- Right-click the new Item, select `Import Data` and then select the Item you want to import.